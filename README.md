# miniproject-scientific-computing2021

This folder contains all the work for Zavier BERTI's miniproject for the SCIENTIFIC COMPUTING course in EFM 2021.

The project is a demonstration of the Monte Carlo integration method for simple two-dimensional functions of the form y=f(x). It is intended as a demonstration tool, as other methods are better suited for such simple functions.

The .ipynb file is best opened in JupyterLab and contains the demo. 

The .py files contain the initial code that can be run in python, but does not have any of the "user friendly" features of the notebook.
