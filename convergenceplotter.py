#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 22 12:29:43 2021

@author: bertiz
"""


def convergenceplot(result):
    import matplotlib.pyplot as plt
    
    fig = plt.figure()
    ax1 = fig.add_subplot(111)
    
    ax1.plot(range(len(result)), result)
    
    ax1.axhline(y=0, color='k')
    ax1.axvline(x=0, color='k')
    ax1.grid()
    plt.show()