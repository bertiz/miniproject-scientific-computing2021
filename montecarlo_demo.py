#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Nov 19 13:30:41 2021

@author: bertiz
"""

def montecarlo_demo(func,xlow,xup,montecarlo_integrator,pointplot,convergenceplot):
    
    [result, xrand, yrand, hitlog] = montecarlo_integrator(func, xlow, xup)
    pointplot(func,xlow,xup,xrand,yrand,hitlog)
    convergenceplot(result)
    return result[-1]
    
    