#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 25 09:32:22 2021

@author: bertiz
"""

#Intake a function and some bounds
def montecarlo_integrator(func, xlow, xup):
    import numpy as np
    #Define max number of shots 
    #and initialize the count of shots that "hit"
    maxshots = 100
    count = 0
    #Initialize vectors to contain iterations
    xrand = np.zeros(maxshots)
    yrand = np.zeros(maxshots)
    yfunc = np.zeros(maxshots)
    hitlog = np.zeros(maxshots)
    result = np.zeros(maxshots)
    #Get a random x value between upper and lower bounds
    #then evaluate the function at that point
    for shot in range(maxshots):
        xrand[shot] = (xup-xlow)*np.random.random()+xlow
        yfunc[shot] = func(xrand[shot])
    #Find the largest and smallest value of the function found in [xlow,xup]
    ymax = max(yfunc)
    ymin = min(yfunc)
    #Treat first the case where the function crosses zero somewhere
    #The total area is then a rectangle from ymin to ymax on [xlow,xup]
    if ymax*ymin < 0:
        totarea = (xup-xlow)*(ymax-ymin)
        #For each shot, pick a random y-value in [ymin,ymax]
        for shot in range(maxshots):
            yrand[shot] = (ymax-ymin)*np.random.random()+ymin
            #Treat first the case where the function is negative at the shot
            if yfunc[shot] < 0:
                #If the shot is above zero or less than the function value
                #the shot "misses"
                if yrand[shot]>0 or yrand[shot] < yfunc[shot]:
                    count = count
                    hitlog[shot] = 0
                #Otherwise, the shot hits, and counts as a negative
                #(it is below zero)
                else:
                    count = count-1
                    hitlog[shot] = -1
            #Case where the function is positive at the shot
            else:
                #If the shot is below zero or bigger than the function value
                #the shot misses
                if yrand[shot]<0 or yrand[shot] > yfunc[shot]:
                    count = count
                    hitlog[shot] = 0
                #Otherwise, the shot hits, and counts as positive
                #(it is above zero)
                else:
                    count = count+1
                    hitlog[shot] = 1
            #The result of the integral after each subsequent shot is taken as
            #the fraction of total shots that hit so far
            result[shot] = totarea*(count/(shot+1))
    #Treat now the case where the function is entirely positive or negative
    #The total area is from zero to the largest abs(y) value on [xlow,xup]
    else:
        totarea = (xup-xlow)*max(abs(ymax),abs(ymin))
        #Treat first the case where the function is entirely positive
        if ymin > 0:
            #For each shot, pick a random y-value in [0,ymax]
            for shot in range(maxshots):
                yrand[shot] = ymax*np.random.random()
                #If the shot is more than the function value
                #the shot "misses"
                if yrand[shot] > yfunc[shot]:
                    count = count
                    hitlog[shot] = 0
                #If the shot is less than the function value the shot hits 
                #and counts as positive (it is above zero)
                else:
                    count = count+1
                    hitlog[shot] = 1
                #The result of the integral after each subsequent shot is taken as
                #the fraction of total shots that hit so far
                result[shot] = totarea*(count/(shot+1))
        #Treat now the case where the function is entirely negative                 
        else:
            #For each shot, pick a random y-value in [ymin,0]
            for shot in range(maxshots):
                yrand[shot] = ymin*np.random.random()
                #If the shot is less than the function value
                #the shot "misses"
                if yrand[shot] < yfunc[shot]:
                    count = count
                    hitlog[shot] = 0
                #If the shot is more than the function value the shot hits 
                #and counts as negative (it is below zero)
                else:
                    count=count-1
                    hitlog[shot] = -1
                #The result of the integral after each subsequent shot is taken as
                #the fraction of total shots that hit so far
                result[shot] = totarea*(count/(shot+1))
    #The function returns the result at each shot, and each shot location            
    return result, xrand, yrand, hitlog
        
    