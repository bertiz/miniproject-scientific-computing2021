#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Nov 19 13:05:44 2021

@author: bertiz
"""


def pointplot(func, xlow, xup, xcoord, ycoord, style):
    import numpy as np
    import matplotlib.pyplot as plt
    
    pos_hitx = []
    pos_hity = []
    neg_hitx = []
    neg_hity = []
    missx = []
    missy = []
    
    for point in range(len(xcoord)):
        if style[point] == 1:
            pos_hitx = np.concatenate((pos_hitx, xcoord[point]), axis = None)
            pos_hity = np.concatenate((pos_hity, ycoord[point]), axis = None)
        else:
            if style[point] == -1:
                neg_hitx = np.concatenate((neg_hitx, xcoord[point]), axis = None)
                neg_hity = np.concatenate((neg_hity, ycoord[point]), axis = None)
            else:
                missx = np.concatenate((missx, xcoord[point]), axis = None)
                missy = np.concatenate((missy, ycoord[point]), axis = None)
    
    xfunc = np.linspace(xlow,xup,num=100)
    yfunc = func(xfunc)
    
    fig = plt.figure()
    ax1 = fig.add_subplot(111)
    
    ax1.plot(xfunc, yfunc)
    
    ax1.plot(pos_hitx, pos_hity, 's', color = "green")
    ax1.plot(neg_hitx, neg_hity, 's', color = "red")
    ax1.plot(missx, missy, 's', color = "gray")
    
    ax1.axhline(y=0, color='k')
    ax1.axvline(x=0, color='k')
    ax1.grid()
    plt.show()
